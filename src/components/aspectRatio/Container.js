import styled from 'styled-components'

const Container = styled('div')`
  width: 70%;
  margin: auto;
`

export default Container
