import styled from 'styled-components'

const MainImage = styled('img')`
  width: 100vw;
  height: 100vh;
  margin: 0;
  padding: 0;
  object-fit: cover;
`

export default MainImage
