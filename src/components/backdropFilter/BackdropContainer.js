import styled from 'styled-components'

const BackdropContainer = styled('div')`
  width: 80vw;
  height: 70vh;
  margin: auto;
  background-color: rgba(256, 256, 256, 0.05);
  backdrop-filter: blur(8px) brightness(120%) saturate(80%);
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

export default BackdropContainer
