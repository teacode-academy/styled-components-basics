import styled from 'styled-components'

const H1 = styled('h1')`
  color: white;
  font-size: 88px;
  text-align: center;
`

export default H1
