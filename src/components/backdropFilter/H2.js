import styled from 'styled-components'

const H2 = styled('h2')`
  color: white;
  font-size: 32px;
`

export default H2
