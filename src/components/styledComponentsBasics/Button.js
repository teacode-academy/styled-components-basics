import styled, { css } from 'styled-components'

const clicked = css`
  box-shadow: 0 0 6px rgba(0,0,0,0.5);
  color: rgba(256,256,256,0.8);
`

const Button = styled('button')`
  background-color: ${props => props.color || '#34b619'};
  padding: 10px 14px;
  border-radius: 16px;
  border: 0;
  color: white;
  text-transform: uppercase;
  outline: 0;
  box-shadow: 0 2px 6px rgba(0,0,0,0.3);
  &:hover {
    box-shadow: 0 3px 6px rgba(0,0,0,0.3);
    ${props => props.clicked && clicked};
  };
`

export default Button
