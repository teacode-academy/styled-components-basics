import styled, { keyframes } from 'styled-components'

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`

const Loader = styled('div')`
  width: 12px;
  height: 12px;
  background: transparent;
  border-radius: 100%;
  border-left: 2px solid ${props => props.color || 'white'};
  border-right: 2px solid ${props => props.color || 'white'};
  animation: ${rotate} 2s linear infinite;
`

export default Loader
