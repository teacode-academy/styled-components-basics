import styled from 'styled-components'

const Container = styled('div')`
  width: 500px;
  height: 500px;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  flex-direction: column;
`

export default Container
