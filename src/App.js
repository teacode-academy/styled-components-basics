import React from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'
import { Route } from 'react-router'
import ButtonPage from './pages/ButtonPage'
import MainPage from './pages/Main'
import AspectRatioPage from './pages/AspectRatio'

function App () {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/button' component={ButtonPage} />
        <Route exact path='/ratio' component={AspectRatioPage} />
        <Route path='/' component={MainPage} />
      </Switch>
    </BrowserRouter>
  )
}

export default App
