import React, { useState } from 'react'
import Button from '../components/styledComponentsBasics/Button'
import Container from '../components/styledComponentsBasics/Container'
import Loader from '../components/styledComponentsBasics/Loader'

const ButtonPage = () => {
  const [counter, setCounter] = useState(0)
  const [clicked, setClicked] = useState({})
  return (
    <Container>
      <Button
        key={1}
        onMouseDown={() => setClicked({ ...clicked, 1: true })}
        onMouseUp={() => setClicked({ ...clicked, 1: false })}
        onClick={() => setCounter(counter + 1)}
        clicked={clicked[1]}
      >
        green
      </Button>
      <Button
        key={2}
        color='#8867d7'
        onClick={() => setCounter(counter + 1)}
        onMouseDown={() => setClicked({ ...clicked, 2: true })}
        onMouseUp={() => setClicked({ ...clicked, 2: false })}
        clicked={clicked[2]}
      >
        purple
      </Button>
      <Button
        key={3}
        color='#23add8'
        onClick={() => setCounter(counter + 1)}
        onMouseDown={() => setClicked({ ...clicked, 3: true })}
        onMouseUp={() => setClicked({ ...clicked, 3: false })}
        clicked={clicked[3]}
      >
        blue
      </Button>
      <Button
        key={4}
        onClick={() => setCounter(counter + 1)}
        onMouseDown={() => setClicked({ ...clicked, 4: true })}
        onMouseUp={() => setClicked({ ...clicked, 4: false })}
        clicked={clicked[4]}
      >
        <Loader />
      </Button>

      <span>Clicked: {counter} times</span>
    </Container>
  )
}

export default ButtonPage
