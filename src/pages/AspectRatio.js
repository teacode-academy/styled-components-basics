import React from 'react'
import Container from '../components/aspectRatio/Container'

const AspectRatioPage = () => {
  return (
    <Container>
      <img
        src='https://res.cloudinary.com/dmflvgiep/image/upload/v1595591135/teacode_tutorials/_IMG2099_qtp5zj.jpg'
        alt='teacode_team'
        width='100%'
        height='100%'
      />
    </Container>
  )
}

export default AspectRatioPage
