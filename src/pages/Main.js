import React from 'react'
import MainImage from '../components/backdropFilter/MainImage'
import BackdropContainer from '../components/backdropFilter/BackdropContainer'
import H1 from '../components/backdropFilter/H1'
import H2 from '../components/backdropFilter/H2'

const MainPage = () => {
  return (
    <>
      <MainImage
        src='https://res.cloudinary.com/dmflvgiep/image/upload/v1595579785/teacode_tutorials/close-up-photo-of-green-leafed-plant-1572036_us23it.jpg'
        alt='main_image'
      />
      <BackdropContainer>
        <H1>Teacode<br />Academy</H1>
        <H2>Learn coding from professionals</H2>
      </BackdropContainer>
    </>
  )
}

export default MainPage
